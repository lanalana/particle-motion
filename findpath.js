var  dataCircle=function(fromX,fromY,dist,flag,xn,yn)
{
        this.fromX=fromX;
        this.fromY=fromY;
        this.dist=dist;
        this.flag=flag;
        this.xn=xn;
        this.yn=yn;
        this.dataCircleStr=function(){
        return this.fromX+" , "+this.fromY+" ; "+this.dist;
    }  

}
      

var Coord=function(x,y){
      this.x=x;
      this.y=y;

    this.StringKoor=function(){
        return this.x+" , "+this.y;
    }  
}

var data;//=[dataCircle];

function findPathfromAtoB(x0,y0,x1,y1,a){
	xnach=x0;
	ynach=y0;
  data=[dataCircle];
      data.length=0;
      var que=[];
      
      que.push(new Coord(x0,y0));
      
         for(var k=0;k<n;k++){
             var innerdata=[];
             data.push(innerdata);
                     for(var r=0;r<m;r++){
                         data[k].push(new dataCircle(-1,-1,0,0));
                     }
                 }
         data[x0][y0].flag = 1;
      

      while(que.length>0){
          var xc=que[0].x;
          var yc=que[0].y;
          que.shift();
          
          var dvoi=[];
           dvoi=dvoich(a[xc][yc]);
           var xn;
           var yn;
           if(dvoi[3]==1){
          xn=xc;
          yn=(yc+1+m)%m;//для того чтобы работал тор
          //console.log('data', data);
          //console.log('xn',xn,'yn',yn);
 

            if(data[xn][yn].flag==0){  
                data[xn][yn].flag=1;
                data[xn][yn].dist=data[xc][yc].dist+1;
                data[xn][yn].fromX=xc;
                data[xn][yn].fromY=yc;
                data[xc][yc].xn=xn;
                data[xc][yc].yn=yn;
                que.push(new Coord(xn,yn));
            }
         }
           
         if(dvoi[1]==1){
          xn=xc;
          //debugger;
          yn=(yc-1+m)%m;

          

            if(data[xn][yn].flag==0){  
                data[xn][yn].flag=1;
                data[xn][yn].dist=data[xc][yc].dist+1;
                data[xn][yn].fromX=xc;
                data[xn][yn].fromY=yc;
                data[xc][yc].xn=xn;
                data[xc][yc].yn=yn;
                que.push(new Coord(xn,yn));
            }
         }
         
         if(dvoi[0]==1){
         xn=(xc+1+n)%n;
          yn=yc;
          
            if(data[xn][yn].flag==0){  
                data[xn][yn].flag=1;
                data[xn][yn].dist=data[xc][yc].dist+1;
                data[xn][yn].fromX=xc;
                data[xn][yn].fromY=yc;
                data[xc][yc].xn=xn;
                data[xc][yc].yn=yn;
                que.push(new Coord(xn,yn));
            }
         }
         
         if(dvoi[2]==1){   
          xn=(xc-1+n)%n;
          yn=yc;
          //console.log('data', data);
          //console.log('xn',xn,'yc',yc,'xc',xc,'m',m);

            if(data[xn][yn].flag==0){  
                data[xn][yn].flag=1;
                data[xn][yn].dist=data[xc][yc].dist+1;
                data[xn][yn].fromX=xc;
                data[xn][yn].fromY=yc;
                data[xc][yc].xn=xn;
                data[xc][yc].yn=yn;
                que.push(new Coord(xn,yn));
            }
         }
      }
      
      var xk=x1;
      var yk=y1;
      var path = [];
      if(data[xk][yk].fromX != -1){
        data[xk][yk].path=1;
        path.push({x: x1, y: y1});
      }
      while((xk!=x0 || yk!=y0) && data[xk][yk].fromX != -1){
          var xnext=data[xk][yk].fromX;
          var ynext=data[xk][yk].fromY;
          xk=xnext;
          yk=ynext;
          data[xk][yk].path=1;
          path.unshift({x: xk, y: yk});
        //  console.log(xk,yk);
          
      }
      
      //если пути нет, то в путь записываем начальные координаты частиц
      if(path.length==0){
		  path.push({x: xnach, y: ynach});
	  }
   // drawpath(data);
   //return data;
   return {data: data, path: path};
  }

  function drawpath(data){
  //console.log(data);
  var kol=0;
    for(var i=0;i<n;i++){
         for(var j=0;j<m;j++){
        if(data[i][j].path==1){
			kol++;
          $("#"+i+'_'+j).css({"background":"green"});

          //var item='[data-x="'+i+'"][data-y="'+j+'"][data-xx="'+data[i][j].xn+'"][data-yy="'+data[i][j].yn+'"] ';
          var item='[data-x="'+data[i][j].fromX+'"][data-y="'+data[i][j].fromY+'"][data-xx="'+i+'"][data-yy="'+j+'"] ';
          //console.log(item);
          $(item).children().css({"background":"green"});
          $(item+" .directionRight").css({"border-left-color":"green"});
          $(item+" .directionTop").css({"border-bottom-color":"green"});
          $(item+" .directionLeft").css({"border-right-color":"green"});
          $(item+" .directionBottom").css({"border-top-color":"green"});
        }
      }
    }  
    return kol;
    //      $("#dl_path").append("<div>Длина пути: "+kol+" </div>"); 
}  
