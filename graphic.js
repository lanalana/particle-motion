function paintGraphic() {
    var numm=0;
    $('#container').highcharts({
        plotOptions: {
            line: { 
                animation: {
                    duration: 7000}
                }
            },    
        title: {
            text: 'График скорости',
            x: -20 //center
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com',
        //     x: -20
        // },
        xAxis: {
            title: {
                text: 'Время'
            },
            tickInterval: 1
        },
        yAxis: {
            title: {
                text: 'Скорость частиц'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        // tooltip: {
        //     valueSuffix: '°C'
        // },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Первая частица',
            data: V[0]
        }, {
            name: 'Вторая частица',
            data: V[1]
        },{
            name: 'Третья частица',
            data: V[2]
        },{
            name: 'Четвертая частица',
            data: V[3]
        },{
            name: 'Пятая частица',
            data: V[4]
        },{
            name: 'Шестая частица',
            data: V[5]
        },{
            name: 'Седьмая частица',
            data: V[6]
        },{
            name: 'Восьмая частица',
            data: V[7]
        // }, {
        //     name: 'London',
        //     data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        // }]
         }]
    });
};


function paintGraphic1() {
    var numm=0;
    $('#container1').highcharts({
        plotOptions: {
            line: { 
                animation: {
                    duration: 7000}
                }
            },    
        title: {
            text: 'График эффективности',
            x: -20 //center
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com',
        //     x: -20
        // },
        xAxis: {
            title: {
                text: 'p'
            },

            categories: [0, 0.1, 0.2, 0.3, 0.4, 0.5,
                0.6, 0.7, 0.8, 0.9, 1]
        },
        yAxis: {
            title: {
                text: 'f'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        // tooltip: {
        //     valueSuffix: '°C'
        // },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '',
            data: Ef
        }
        // }, {
        //     name: 'London',
        //     data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        // }]
         ]
    });
};

function paintGraphic2() {
    var numm=0;
    $('#container2').highcharts({
        plotOptions: {
            line: { 
                animation: {
                    duration: 7000}
                }
            },    
        title: {
            text: 'График движения',
            x: -20 //center
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com',
        //     x: -20
        // },
        xAxis: {
            title: {
                text: 'Время'
            },
            tickInterval: 1
        },
        yAxis: {
            title: {
                text: 'Скорость частиц'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        // tooltip: {
        //     valueSuffix: '°C'
        // },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Первая частица',
            data: V[0]
        }, {
            name: 'Вторая частица',
            data: V[1]
        },{
            name: 'Третья частица',
            data: V[2]
        },{
            name: 'Четвертая частица',
            data: V[3]
        },{
            name: 'Пятая частица',
            data: V[4]
        },{
            name: 'Шестая частица',
            data: V[5]
        },{
            name: 'Седьмая частица',
            data: V[6]
        },{
            name: 'Восьмая частица',
            data: V[7]
        // }, {
        //     name: 'London',
        //     data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        // }]
         }]
    });
};
